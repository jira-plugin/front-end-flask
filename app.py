from flask import Flask, render_template, jsonify, redirect, request, url_for

from jinja2 import Template


app = Flask(__name__)

# atlassian-connect.json

descriptor = {
    "name": "AIIC",
    "description": "Atlassian Connect app",
    "key": "com.example.myapp",
    "baseUrl": "https://03a66506cdb7.ngrok.io",
    "vendor": {
        "name": "Example, Inc.",
        "url": "http://example.com"
    },
    "authentication": {
        "type": "none"
    },
    "apiVersion": 1,
    "modules": {
        "generalPages": [
            {
                "url": "/helloworld.html",
                "key": "hello-world",
                "location": "system.top.navigation.bar",
                "name": {
                    "value": "Greeting"
                }
            }
        ],
        "webPanels": [
            {
                "key": "example-issue-right-panel",
                "location": "atl.jira.view.issue.right.context",
                "name": {
                    "value": "Get Related Links"
                },
                "url": "/relatedConfluenceDiscussions.html"
            }
        ],
        "webItems": [
            {
                "location": "jira.software.board.tools",
                "weight": 200,
                "styleClasses": [
                    "webitem",
                    "system-present-webitem"
                ],
                "url": "/helloworld.html",
                "context": "addon",
                "tooltip": {
                    "value": "Give Discussion Link to generate issues automatically"
                },
                "icon": {
                    "width": 24,
                    "height": 24,
                    "url": "/static/img/info.png"
                },
                "name": {
                    "value": "Create Issues"
                },
                "key": "web-item-board"
            },
            {
                "key": "example-tools-item",
                "location": "jira.issue.tools",
                "weight": 200,
                "name": {
                    "value": "Hello world"
                },
                "url": "/example-section-link"
            }
        ],
        "jiraProjectPages": [
            {
                "key": "generate-issues",
                "name": {
                    "value": "Generate Issues"
                },
                "url": "/helloworld.html",
                "iconUrl": "main.png",
                "weight": 1
            }
        ],
        # "dialogs": [
        #     {
        #         "url": "/static/index.html?board_id={board.id}",
        #         "options": {
        #             "chrome": "false",
        #             "header": {
        #                 "value": "Hello-World!"
        #             },
        #             "size": "x-large"
        #         },
        #         "key": "dialog-module-readme"
        #     }
        # ],
    }
}

api_base = {
    "baseUrl": "https://ab7ec13e2e7e.ngrok.io"
}


@app.route('/')
def app_descriptor():
    return jsonify(descriptor)


@app.route('/relatedConfluenceDiscussions.html')
def relatedConfluenceDiscussions():
    return render_template('relatedConfluenceDiscussions.html', data=api_base)


@app.route('/helloworld.html')
def helloworld():
    return render_template('helloworld.html', data=api_base)


@app.route('/recommendedIssues.html')
def recommendedIssues():
    return render_template('recommendedIssues.html', data=api_base)


@app.route('/redirect-to-recommendedIssues')
def redirect_to_recommendedIssues():
    return redirect(url_for('recommendedIssues'))

# @app.route('/test')
# def test():
#     return jsonify({'JWT': 'this is Jwt!'})


@app.route('/post2jira', methods=['GET', 'POST'])
def post2Jira():
    issue_data = request.get_json()

    print(issue_data)
    return jsonify({"Key": 1077, "issue": issue_data})


if __name__ == "__main__":
    app.run(debug=True)


//configuring Firebase
var firebaseConfig = {
    apiKey: "AIzaSyAWb-wsfXyDkbnDhLePG_Fny5aMt4HVqZc",
    authDomain: "jira-plugin-992bd.firebaseapp.com",
    databaseURL: "https://jira-plugin-992bd.firebaseio.com",
    projectId: "jira-plugin-992bd",
    storageBucket: "jira-plugin-992bd.appspot.com",
    messagingSenderId: "1057372609588",
    appId: "1:1057372609588:web:3aecde9e30a3b9e2d4be61",
    measurementId: "G-P0WVE5QCCC"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);


//reference url collection
var urlRef = firebase.database().ref('discussionLinks');

//listen for form submit event
//document.getElementById('discussionUrlForm').addEventListener('submit', getIssues);

//generate issues by sending url as a parameter
function getIssues(e) {
    e.preventDefault();

    // get form values i.e, url
    var url = document.getElementById('discussion_link').value;
    //console.log(url)
    saveUrl(url);
}

//post the url to firebase; for testing purpose
function saveUrl(url) {
    var newUrlRef = urlRef.push();
    newUrlRef.set({
        discussion_url: url
    });
}


var parentPageUrl = ""
window.addEventListener('load', async function getParentPageUrl(event) {
    parentPageUrl = location;
    let retrieveUrlStatus = new Promise((resolve, reject) => {
        AP.getLocation(function (location) {
            //alert(location);
            console.log(location);
            parentPageUrl = location
            resolve("parent container url retrieved");
        });
    });

    let statusResult = await retrieveUrlStatus;

    console.log(statusResult);
    console.log(parentPageUrl);
    var confluenceUrl = createConfluenceUrl(parentPageUrl);

    document.getElementById("discussion-frame").setAttribute("src", confluenceUrl);
    // document.getElementById("confluenceUrl").setAttribute("href", confluenceUrl);
    // issueId = retrieveIssueId(parentPageUrl);
    // console.log(issueId);
})

function createConfluenceUrl(parentPageUrl) {
    var confluenceUrl = ""
    var parentPageUrl = parentPageUrl.split("");
    for (i = 0; i < parentPageUrl.length; i++) {
        // if (i == 0 && parentPageUrl[i] == "/")
        //     continue;
        // else 
        if (i != 0 && confluenceUrl.includes("atlassian.net/"))
            break;
        else {
            confluenceUrl += parentPageUrl[i]
        }
    }
    confluenceUrl += "wiki"
    console.log("confluence url: " + confluenceUrl)
    return confluenceUrl;
}

//lisent to select discussion button on "selecting directly from confluence tab"
document.getElementById("getConfluenceUrl").addEventListener("click", () => {
    console.log(window.location.href);
    // console.log(document.getElementById("discussion-frame").contentDocument.referrer);
    //console.log(document.getElementById("discussion-frame").contentWindow.location.href);
})


//collecting the url directly from the user to create isseus

document.addEventListener('DOMContentLoaded', () => {
    document
        .getElementById('discussionUrlForm')
        .addEventListener('submit', (event) => {
            event.preventDefault();

            const discussionInput = document.getElementById("discussion_link");

            var errorDiv = document.getElementById("error-message");

            console.log(discussionInput.value);

            if (!discussionInput.value.includes("https://confluence")) {
                discussionInput.value = "";
                errorDiv.innerHTML = "** PLease Provide a valid Confluence Url";
                return;
            }

            localStorage.setItem("selectedDiscussion", discussionInput.value);

            discussionInput.value = "";

            window.location.replace("recommendedIssues.html");

        });
});

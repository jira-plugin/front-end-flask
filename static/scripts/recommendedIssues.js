document.addEventListener('DOMContentLoaded', () => {
    document
        .getElementById('issueEditForm')
        .addEventListener('submit', postData2Backend);
});

async function postData2Backend(event) {

    var postButton = document.getElementById("postData2Jira");
    postButton.disabled = true;
    var spinner = document.getElementById("createIssueSpinner");
    spinner.style.display = "inline-flex";

    event.preventDefault();

    let myform = event.target;

    let fd = new FormData(myform);

    // let url = "https://1b0d304f9ba5.ngrok.io/issuegen/submit";
    let url = "/post_issue2jira"

    let header = new Headers();

    header.append("content-type", "application/json");

    let json = await convertFD2JSON(fd);
    console.log(json);
    let req = new Request(url, {
        headers: header,
        body: json,
        method: "POST"
    });

    const response = await fetch(req);

    const data = await response.json();

    console.log(data);
    console.log(data.Key);

    const issueUrl = `https://atc3360.atlassian.net/browse/${data.Key}`;


    //making the selected issue card dissapear
    var selectedIssueId = localStorage.getItem("selectedIssueId");
    document.getElementById(selectedIssueId).parentElement.parentElement.style.display = "none";
    // console.log(document.getElementById(selectedIssueId));
    // console.log(document.getElementById(selectedIssueId).parentElement);
    // console.log(document.getElementById(selectedIssueId).parentElement.parentElement);
    // event.currentTarget.parentElement.style.display = "none";
    // event.currentTarget.parentElement.parentElement.style.display = "none";

    //set create message
    document.getElementById("issueCreateMessage").innerHTML = "Your Issue has been created and added to the project backlog";
    //set create url
    document.getElementById("issueCreateUrl").href = issueUrl;
    document.getElementById("issueCreateUrl").setAttribute("target", "_blank");
    //finally display the modal
    $("#issueCreateStatus").modal("show");


    spinner.style.display = "none";
    postButton.disabled = false;

    $("#issueEditModal").modal("hide");

    setTimeout(() => { $("#issueCreateStatus").modal("hide"); }, 10000);
}

function convertFD2JSON(formData) {
    let obj = {};
    for (let key of formData.keys()) {
        obj[key] = formData.get(key);
    }
    return JSON.stringify(obj);
}
